# StackOverflow Users App

## Overview

The app has functionality for fetching a list of users and displaying them as a list with their profile image, username and reputation visible.

When a cell is tapped, the cell expands to reveal a "Follow" and "Block" button. Following the user changes the cell to have a disclosure indicator. Blocking the user displays the cell in a "disabled" state. Both the actions are reversable. Tapping the cell again will hide the buttons. All following and blocking is locally simulated. 

NOTE - Given there is no detail view being built in this challenge, I have as per the requirements built a cell that expands / collapses upon being tapped. In the long run, it would make sense to have a button to press which expands / collapses the cell, so tapping on the cell itself could trigger a segue to a details page.


## Architecture

The App is constructed of 4 targets. The main “StackOverflowApp” target itself, and then 3 frameworks:

1. Images - for image fetching
2. Plexus - for general networking behaviour
3. UserList - for providing the screens and functionality for the users list page (i.e. a feature specific framework)

All of these frameworks are individually compilable and testable via their schemes, allowing for easy development (quick build and test times) on these frameworks if the app were to continually grow.


## Frameworks

### UserList - 
For any code specific to the UserList page itself.

An MVVM approach is used within the UsersList feature. The flow of data is as follows:

Server (API) -> API Models -> App Models -> ViewModels (passed in for configuring view)

I used this approach as it allowed me to separate networking out of the cells / viewcontroller a bit more simply for testability purposes. Also, due to the local simulation of follow / block, I needed to use a class for the reference semantics. It seemed preferable to me to contain these reference semantics within the viewmodel and therefore be able to pass basic structs along for the user objects themselves.


### Plexus - 
Plexus provides a mechanism for networking within the app.

The concept of a "Resource" is used - breaking down networking behaviour into a URLRequest and a transform from Data -> Value upon fetching data from the server.

Codable is utilised, taking JSON data into "API layer" structs. The API level structs are then mapped to "App layer" structs. The benefit of this is it allows you to apply loose data rules on the decode (i.e. lots of optional properties) but then apply more specific data rules on the mapping from API -> App models. During this mapping you can choose to throw very specific errors or make certain model transformartion rules, which can make error handling easier and should allow for more valid data to come through.

A generic fetch(resource: _ completion:) exists as an extension on URLSession for performing a fetch based upon a resource. This means that if new API calls were to be added, you can simply make another resource and use the existing pipeline. 

NOTE - the transformation pipeline for a resource is easy to test and allows you to have confidence in your data transformation from the API (see FetchUsersResourceTests.swift)

Within Plexus, there is a public URLSession, which is able to be mocked internally for testing. The benefit of using a globally available session like this is you could set something up to stub it in tests to stop network requests being made. With more time, I would build a MockURLSession which can return data you give it to allow further testing across different API calls.


### Images - 
The images framework is designed to provide a global way for fetching images, to save different implementations being built throughout the app. Again, there is a URLSession which can be stubbed for testing.
