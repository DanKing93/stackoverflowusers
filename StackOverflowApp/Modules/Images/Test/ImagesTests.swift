//
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest
@testable import Images

class ImagesTests: XCTestCase {

    func test_somethingToCompile() {
        let string = "String"
        XCTAssertEqual(string, "String")
    }
}
