//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit
import Plexus

public struct ImageFetcher {
    
    static var urlSession: URLSession = URLSession.shared
    
    /// Fetches image data and coverts to a UIImage using the provided transform
    /// - Parameters:
    ///   - resource: Resource with a transform from data and url response to UIImage
    ///   - completion: Callback after wrapping successfully created UIImage or thrown error in Result
    public static func fetch<Value: UIImage>(resource: Resource<Value>, _ completion: @escaping (Result<Value, Error>) -> Void) -> URLSessionDataTask {
        
        let task = urlSession.dataTask(with: resource.request) { (data, response, error) in
            
            DispatchQueue.main.async {
                
                guard let data = data, let response = response else {
                    completion(.failure(RequestError()))
                    return
                }
                
                let result = Result(catching: { try resource.transform((data, response)) })
                completion(result)
                
            }
        }
        
        task.resume()
        return task
    }
}
