//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit
import Plexus

extension Resource where Value == UIImage {
    
    public static func image(for request: URLRequest) -> Resource<UIImage> {
        
        return Resource(request: request) { (data, response) -> UIImage in
            
            guard let image = UIImage(data: data) else {
                throw ImageLoadError()
            }
            
            return image
        }
    }
}

public struct ImageLoadError: Swift.Error { }
