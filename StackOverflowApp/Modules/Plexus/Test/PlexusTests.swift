//
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest
@testable import Plexus

class PlexusTests: XCTestCase {

    func test_somethingToCompile() {
        let string = "String"
        XCTAssertEqual(string, "String")
    }
}
