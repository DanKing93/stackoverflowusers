//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension URLSession {
    
    /// URLSession to be used for network calls within the app
    /// For testing, this can be overidden with a mock to stop real
    /// network requests being made
    public internal(set) static var sharedSession = URLSession.shared
}
