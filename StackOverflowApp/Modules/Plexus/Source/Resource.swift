//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

/// Resource for fetching data from network
public struct Resource<Value> {
    
    public typealias Response = (Data, URLResponse)
    
    /// URLRequest to fetch with
    public let request: URLRequest
    
    /// Function transforming data and URLResponse obtained from
    /// network request into a value
    public let transform: (Response) throws -> Value
    
    public init(request: URLRequest,
                transform: @escaping (Response) throws -> Value) {
        self.request = request
        self.transform = transform
    }
}

extension Resource {
    
    public func tryMap<NewValue>(_ newTransform: @escaping (Value) throws -> NewValue) -> Resource<NewValue> {
        return Resource<NewValue>(request: self.request) {
            let value = try self.transform($0)
            return try newTransform(value)
        }
    }
}
