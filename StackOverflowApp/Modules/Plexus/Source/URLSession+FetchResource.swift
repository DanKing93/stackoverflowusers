//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension URLSession {
    
    /// Fetches data using the request from the provided resource and
    /// attempts to convert data into value using provided transform on resource
    /// - Parameters:
    ///   - resource: Resource providing request and transformation function
    ///   - completion: Callback after wrapping successfully create value or thrown error in Result
    public func fetch<Value>(_ resource: Resource<Value>, _ completion: @escaping (Result<Value, Error>) -> Void) -> URLSessionDataTask {
        
        let task = dataTask(with: resource.request) { (data, response, error) in
            
            guard let data = data,
                let response = response else {
                    completion(.failure(RequestError()))
                    return
            }
            
            let result = Result(catching: { try resource.transform((data, response)) })
            completion(result)
        }
        
        task.resume()
        return task
    }
}

public struct RequestError: Error {
    public init() { }
}
