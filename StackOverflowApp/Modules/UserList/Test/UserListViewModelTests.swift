//
//  Copyright © 2020 Dan King. All rights reserved.
//

@testable import UserList
import XCTest

class UserListViewModelTests: XCTestCase {

    func test_loadView_success_callsDidFetchUsersOnView() {
        
        let usersResponse = [User(name: "User 1", reputation: 1000, profileImageURL: nil),
                             User(name: "User 2", reputation: 2000, profileImageURL: nil)]
        
        let mockInteractor = MockInteractor(usersToReturn: usersResponse)
        let mockView = MockUsersListView()
        
        let viewModel = UsersListViewModel(usersView: mockView, interactor: mockInteractor)
        
        viewModel.load()
        assertTrueWaiting({ mockView.didFetchUsersWasCalled }, timout: 2.0)
    }
    
    func test_loadView_error_callsDidFailToFetchUsersCalled() {
        
        struct FakeError: Swift.Error { }
        
        let mockInteractor = MockInteractor(errorToReturn: FakeError())
        let mockView = MockUsersListView()
        
        let viewModel = UsersListViewModel(usersView: mockView, interactor: mockInteractor)
        
        viewModel.load()
        assertTrueWaiting({ mockView.didFailToFetchUsersWasCalled }, timout: 2.0)
    }
    
    func test_noOfRows_itemForRow() {
        
        let usersResponse = [User(name: "User 1", reputation: 1000, profileImageURL: nil),
                             User(name: "User 2", reputation: 2000, profileImageURL: nil)]
        
        let mockInteractor = MockInteractor(usersToReturn: usersResponse)
        let mockView = MockUsersListView()
        
        let viewModel = UsersListViewModel(usersView: mockView, interactor: mockInteractor)
        
        viewModel.load()
        assertTrueWaiting({ mockView.didFetchUsersWasCalled }, timout: 2.0)
        
        XCTAssertEqual(viewModel.numberOfRows(inSection: 0), 2)
        
        let expectedCellModel = UserCellViewModel(name: "User 1", reputation: 1000, imageURL: nil)
        let actual = viewModel.itemForRow(at: IndexPath(row: 0, section: 0))
        XCTAssertEqual(expectedCellModel.name, actual.name)
        XCTAssertEqual(expectedCellModel.reputation, actual.reputation)
        XCTAssertEqual(expectedCellModel.imageURL, actual.imageURL)
    }
}

extension UserListViewModelTests {
    
    private func assertTrueWaiting(_ expression: @escaping () -> Bool,
                                   timout: Double) {
        
        let predicate = NSPredicate { (_, _) -> Bool in
            return expression()
        }
        
        let exp = expectation(for: predicate, evaluatedWith: nil, handler: nil)
        wait(for: [exp], timeout: 2.0)
        
        XCTAssertTrue(expression())
    }
}
