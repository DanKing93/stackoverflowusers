//
//  Copyright © 2020 Dan King. All rights reserved.
//

@testable import UserList
import XCTest

class UserTableViewCellTests: XCTestCase {
    
    let mockNameLabel = UILabel()
    let mockReputationLabel = UILabel()
    let mockImageView = UIImageView()
    
    let mockBlockButton = UIButton()
    let mockFollowButtom = UIButton()
    let mockActionsView = UIStackView()

    func test_cell_configuresWithViewModel() {
        
        let cell = configuredCell()
        let viewModel = UserCellViewModel(name: "Name", reputation: 1000, imageURL: nil)
        
        cell.configure(withViewModel: viewModel)
        
        XCTAssertEqual(cell.nameLabel.text, "Name")
        XCTAssertEqual(cell.reputationLabel.text, "Reputation: 1000")
        XCTAssertEqual(cell.accessoryType, .none)
        
        XCTAssertEqual(cell.actionsView.isHidden, true)
        XCTAssertEqual(mockBlockButton.titleLabel?.text, "Block")
        XCTAssertEqual(mockFollowButtom.titleLabel?.text, "Follow")
    }
    
    func test_cell_prepareForReuse_resets() {
        
        let cell = configuredCell()
        let viewModel = UserCellViewModel(name: "Name", reputation: 1000, imageURL: nil)
        
        // Simulate initial use
        cell.configure(withViewModel: viewModel)
        
        cell.prepareForReuse()
        
        XCTAssertEqual(cell.accessoryType, .none)
        XCTAssertEqual(cell.actionsView.isHidden, true)
    }
    
    func test_cellExpandsWhenTapped() {
        
        let cell = configuredCell()
        
        cell.toggleDisplay()
        XCTAssertEqual(cell.actionsView.isHidden, false)
    }
    
    func test_cell_updatesView_blocked() {
        
        let cell = configuredCell()
        let viewModel = UserCellViewModel(name: "Name", reputation: 1000, imageURL: nil)
        cell.configure(withViewModel: viewModel)
        
        cell.didTapBlock()
        
        XCTAssertEqual(cell.accessoryType, .none)
        XCTAssertEqual(mockBlockButton.titleLabel?.text, "Unblock")
        XCTAssertEqual(mockFollowButtom.titleLabel?.text, "Follow")
    }
    
    func test_cell_updatesView_followed() {
        
        let cell = configuredCell()
        let viewModel = UserCellViewModel(name: "Name", reputation: 1000, imageURL: nil)
        cell.configure(withViewModel: viewModel)
        
        cell.didTapFollow()
        
        XCTAssertEqual(cell.accessoryType, .disclosureIndicator)
        XCTAssertEqual(mockBlockButton.titleLabel?.text, "Block")
        XCTAssertEqual(mockFollowButtom.titleLabel?.text, "Unfollow")
    }
    
    func test_cell_updatesView_unfollowed() {
        
        let cell = configuredCell()
        let viewModel = UserCellViewModel(name: "Name", reputation: 1000, imageURL: nil)
        cell.configure(withViewModel: viewModel)
        
        // Simulate follow
        cell.didTapFollow()
        
        // Simulate unfollow
        cell.didTapFollow()
        
        XCTAssertEqual(cell.accessoryType, .none)
        XCTAssertEqual(mockBlockButton.titleLabel?.text, "Block")
        XCTAssertEqual(mockFollowButtom.titleLabel?.text, "Follow")
    }
    
    // MARK: - Helpers
    
    func configuredCell() -> UserTableViewCell {
        
        let cell = UserTableViewCell()
        
        cell.nameLabel = mockNameLabel
        cell.reputationLabel = mockReputationLabel
        cell.profileImageView = mockImageView
        cell.actionsView = mockActionsView
        cell.followButton = mockFollowButtom
        cell.blockButton = mockBlockButton
        
        cell.awakeFromNib()
        return cell
    }
}
