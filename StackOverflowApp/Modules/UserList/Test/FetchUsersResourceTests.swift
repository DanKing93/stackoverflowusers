//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Plexus
import UserList
import XCTest

class FetchUsersResourceTests: XCTestCase {
    
    // MARK: - Request
    
    func test_resource_request() {
        
        let resource = Resource.fetchUsers
        
        XCTAssertEqual(resource.request.url?.absoluteString, "http://api.stackexchange.com/2.2/users?pagesize=20&order=desc&sort=reputation&site=stackoverflow")
    }
    
    // MARK - Success
    
    func test_transform() throws {
        
        let data = try loadJSON(withFilename: "ValidUsersResponse")
        let resource = Resource.fetchUsers
        
        let value = try resource.transform((data, HTTPURLResponse(statusCode: 200)))
        XCTAssertEqual(value.count, 5)
    }
    
    func test_transform_excludesInvalidUsers() throws {
        
        let data = try loadJSON(withFilename: "PartInvalidUsersResponse")
        let resource = Resource.fetchUsers
        
        let value = try resource.transform((data, HTTPURLResponse(statusCode: 200)))
        XCTAssertEqual(value.count, 2)
        
        let expectedUserArray = [User(name: "Darin Dimitrov",
                                      reputation: 903265,
                                      profileImageURL: URL(string: "https://www.gravatar.com/avatar/e3a181e9cdd4757a8b416d93878770c5?s=128&d=identicon&r=PG")!),
                                 User(name: "Marc Gravell",
                                 reputation: 853401,
                                 profileImageURL: URL(string: "https://i.stack.imgur.com/CrVFH.png?s=128&g=1")!)
                                ]
        
        XCTAssertEqual(value, expectedUserArray)
    }
    
    // MARK: - Errors
    
    func test_transform_httpError_errorThrown() throws {
        
        let data = try loadJSON(withFilename: "ValidUsersResponse")
        let resource = Resource.fetchUsers
        
        XCTAssertThrowsError(try resource.transform((data, HTTPURLResponse(statusCode: 500))), "Error thrown") { error in
            guard let fetchError = error as? FetchUsersError else { XCTFail("Wrong error type"); return }
            XCTAssertEqual(fetchError, FetchUsersError(statusCode: 500))
        }
    }
}

extension FetchUsersResourceTests {

    // MARK: - Helpers

    private func loadJSON(withFilename filename: String) throws -> Data {

        let bundle = Bundle(for: FetchUsersResourceTests.self)

        guard let path = bundle.path(forResource: filename, ofType: "json"),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) else {
                XCTFail("Couldn't generate JSON Data")
                throw JSONError()
        }

        return data
    }
}

fileprivate struct JSONError: Error { }

fileprivate extension HTTPURLResponse {

    convenience init(statusCode: Int) {
        self.init(url: URL(string: "https://www.google.com")!,
                  statusCode: statusCode,
                  httpVersion: nil,
                  headerFields: nil)!
    }
}

