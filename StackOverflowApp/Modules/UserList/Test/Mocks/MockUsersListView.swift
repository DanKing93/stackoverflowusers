//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UserList
import Foundation

class MockUsersListView: UsersListView {
    
    var didFetchUsersWasCalled = false
    func didFetchUsers() {
        didFetchUsersWasCalled = true
    }
    
    var didFailToFetchUsersWasCalled = false
    func didFailToFetchUsers() {
        didFailToFetchUsersWasCalled = true
    }
}
