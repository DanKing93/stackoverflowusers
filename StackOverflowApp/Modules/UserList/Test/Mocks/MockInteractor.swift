//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UserList
import Foundation

struct MockInteractor: UserListInteractor {
    
    private var usersToReturn: [User]
    private var errorToReturn: Swift.Error?
    
    init(usersToReturn: [User] = [],
         errorToReturn: Swift.Error? = nil) {
        self.usersToReturn = usersToReturn
        self.errorToReturn = errorToReturn
    }
    
    func fetchUsers(_ completion: @escaping (Result<[User], Error>) -> Void) {
        
        if let error = errorToReturn {
            completion(.failure(error))

        } else {
            completion(.success(usersToReturn))
        }
    }
}
