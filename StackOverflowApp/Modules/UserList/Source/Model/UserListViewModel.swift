//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

protocol UsersViewModel {
    func load()
    func numberOfRows(inSection section: Int) -> Int
    func itemForRow(at indexPath: IndexPath) -> UserCellViewModel
}

class UsersListViewModel: UsersViewModel {
    
    private var users: [UserCellViewModel] = []
    private var interactor: UserListInteractor
    
    private weak var usersView: UsersListView?
    
    init(usersView: UsersListView,
         interactor: UserListInteractor = DefaultUserListInteractor()) {
        self.usersView = usersView
        self.interactor = interactor
    }
    
    func load() {
        
        interactor.fetchUsers() { [weak self] result in
            
            guard let self = self else { return }
            
            switch result {
                
            case .success(let users):
                self.users = users.map(UserCellViewModel.init)
                self.usersView?.didFetchUsers()
                
            case .failure(_):
                self.usersView?.didFailToFetchUsers()
            }
        }
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        users.count
    }
    
    func itemForRow(at indexPath: IndexPath) -> UserCellViewModel {
        users[indexPath.row]
    }
}
