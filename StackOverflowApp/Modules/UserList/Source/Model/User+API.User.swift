//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension User {
    
    init?(_ user: API.User) {
        
        guard let name = user.displayName,
            name != "",
            let reputation = user.reputation else {
                return nil
        }
        
        var imageURL: URL? = nil
        if let profileURL = user.profileImageURL {
            imageURL = URL(string: profileURL)
        }
        
        self.init(name: name,
                  reputation: reputation,
                  profileImageURL: imageURL)
    }
}
