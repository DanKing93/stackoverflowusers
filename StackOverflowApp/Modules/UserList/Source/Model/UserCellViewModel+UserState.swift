//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension UserCellViewModel {
    
    enum UserState {
        case none
        case followed
        case blocked
    }
}
