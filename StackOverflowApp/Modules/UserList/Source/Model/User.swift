//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

public struct User {
    let name: String
    let reputation: Int
    let profileImageURL: URL?
    
    public init(name: String,
                reputation: Int,
                profileImageURL: URL?) {
        self.name = name
        self.reputation = reputation
        self.profileImageURL = profileImageURL
    }
}

extension User: Equatable { }
