//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension Array where Element == User {
    
    init(_ userResponse: API.UsersResponse) {
        
        guard let apiUsers = userResponse.users else {
            self = []
            return
        }
        
        self = apiUsers.compactMap { User($0) }
    }
}
