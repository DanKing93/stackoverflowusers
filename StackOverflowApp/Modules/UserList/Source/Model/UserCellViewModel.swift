//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit
import Images

class UserCellViewModel {
    
    let name: String
    let reputation: Int
    let imageURL: URL?
    
    var userState: UserState = .none
    
    init(name: String,
         reputation: Int,
         imageURL: URL?) {
        self.name = name
        self.reputation = reputation
        self.imageURL = imageURL
    }
}

extension UserCellViewModel {
    
    convenience init(_ user: User) {
        
        self.init(name: user.name,
                  reputation: user.reputation,
                  imageURL: user.profileImageURL)
    }
}
