//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Images
import UIKit

extension UserCellViewModel {
    
    func fetchImage(_ completion: @escaping (UIImage?) -> Void) -> URLSessionDataTask? {
        
        guard let url = self.imageURL else {
            completion(nil)
            return nil
        }
        
        let request = URLRequest(url: url)
        
        let task = ImageFetcher.fetch(resource: .image(for: request)) { result in
            
            if case .success(let image) = result {
                completion(image)
            }
        }
        
        return task
    }
}
