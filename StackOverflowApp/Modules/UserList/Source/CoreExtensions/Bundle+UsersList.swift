//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension Bundle {
    
    static var usersList: Bundle {
        class BundleFinder { }
        return Bundle(for: BundleFinder.self)
    }
}
