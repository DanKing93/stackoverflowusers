//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var adaptiveDisabled: UIColor {
        
        if #available(iOS 13, *) {
            
            return UIColor { traitCollection -> UIColor in
                    
                if traitCollection.userInterfaceStyle == .dark {
                    return UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 0.15)
                
                } else {
                    return UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 0.8)
                }
            }
            
        } else {
            return UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 0.8)
        }
    }
}
