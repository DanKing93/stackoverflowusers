//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit

/// For displaying a loading view with an Activity Indicator
public class LoadingView: UIView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    /// Make loading view visible and animate activity indicator
    public func startLoading() {
        isHidden = false
        activityIndicator.startAnimating()
    }
    
    /// Hide loading view and stop animating activity indicator
    public func stopLoading() {
        activityIndicator.stopAnimating()
        isHidden = true
    }
}
