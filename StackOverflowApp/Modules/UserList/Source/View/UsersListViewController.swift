//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit

public protocol UsersListView: class {
    func didFetchUsers()
    func didFailToFetchUsers()
}

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var tableView: UITableView!
    
    // Internal for mocking in test
    internal var viewModel: UsersViewModel! {
        didSet {
            loadingView.startLoading()
            viewModel.load()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "StackOverflow Users"
        
        viewModel = UsersListViewModel(usersView: self)
        
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.usersList), forCellReuseIdentifier: "UserTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    @IBAction func didTapRetry() {
        errorView.isHidden = true
        loadingView.startLoading()
        viewModel.load()
    }
}

// MARK: - UsersListView

extension UsersListViewController: UsersListView {
    
    func didFailToFetchUsers() {
        loadingView.stopLoading()
        errorView.isHidden = false
    }
    
    func didFetchUsers() {
        loadingView.stopLoading()
        tableView.reloadData()
    }
}

// MARK: - UITableViewDataSource

extension UsersListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellViewModel = viewModel.itemForRow(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as! UserTableViewCell
        cell.configure(withViewModel: cellViewModel)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension UsersListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        toggleCell(atIndexPath: indexPath)
    }
    
    func toggleCell(atIndexPath indexPath: IndexPath) {
        
        guard let userCell = tableView.cellForRow(at: indexPath) as? UserTableViewCell else { return }
        userCell.toggleDisplay()
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
