//
//  Copyright © 2020 Dan King. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var reputationLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var actionsView: UIStackView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var blockButton: UIButton!
    
    private var viewModel: UserCellViewModel = UserCellViewModel(name: "", reputation: 0, imageURL: nil)
    
    private var task: URLSessionDataTask? {
        willSet { task?.cancel() }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
        actionsView.isHidden = true
        
        followButton.backgroundColor = .systemGreen
        followButton.setTitle("Follow", for: .normal)
        followButton.layer.cornerRadius = 10
        
        blockButton.backgroundColor = .systemRed
        blockButton.setTitle("Block", for: .normal)
        blockButton.layer.cornerRadius = 10
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        task = nil
        profileImageView.image = UIImage(named: "profile", in: Bundle.usersList, compatibleWith: nil)
        viewModel = UserCellViewModel(name: "", reputation: 0, imageURL: nil)
        actionsView.isHidden = true
        accessoryType = .none
        backgroundColor = .none
    }
    
    func configure(withViewModel viewModel: UserCellViewModel) {
        
        self.viewModel = viewModel
        nameLabel.text = viewModel.name
        reputationLabel.text = "Reputation: \(String(viewModel.reputation))"
        
        updateActionView()
        
        task = viewModel.fetchImage { [weak self] image in
            self?.profileImageView.image = image
        }
    }
    
    private func updateActionView() {
        
        switch viewModel.userState {
            
        case .blocked:
            blockButton.setTitle("Unblock", for: .normal)
            followButton.setTitle("Follow", for: .normal)
            backgroundColor = .adaptiveDisabled
            accessoryType = .none
            
        case .followed:
            blockButton.setTitle("Block", for: .normal)
            followButton.setTitle("Unfollow", for: .normal)
            backgroundColor = .none
            accessoryType = .disclosureIndicator
        
        case .none:
            blockButton.setTitle("Block", for: .normal)
            followButton.setTitle("Follow", for: .normal)
            backgroundColor = .none
            accessoryType = .none
        }
    }
    
    // MARK: - Sent actions
    
    @IBAction func didTapFollow() {
        
        if viewModel.userState == .followed {
            viewModel.userState = .none
            
        } else {
            viewModel.userState = .followed
        }
        
        updateActionView()
    }
    
    @IBAction func didTapBlock() {
        
        if viewModel.userState == .blocked {
            viewModel.userState = .none
        
        } else {
            viewModel.userState = .blocked
        }
        
        updateActionView()
    }
    
    func toggleDisplay() {
        actionsView.isHidden.toggle()
    }
}
