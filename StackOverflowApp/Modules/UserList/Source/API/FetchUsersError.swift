//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

public struct FetchUsersError: Swift.Error {
    
    let statusCode: Int
    
    public init(statusCode: Int) {
        self.statusCode = statusCode
    }
    
    init?(urlResponse: URLResponse) {
        
        guard let httpResponse = urlResponse as? HTTPURLResponse else { return nil }
        guard (400..<599).contains(httpResponse.statusCode) else { return nil }
        
        self.init(statusCode: httpResponse.statusCode)
    }
}

extension FetchUsersError: Equatable { }
