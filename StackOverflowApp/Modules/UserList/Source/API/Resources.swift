//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation
import Plexus

extension Resource where Value == [User] {
    
    public static var fetchUsers: Resource<[User]> {
        API.users.tryMap(Array<User>.init)
    }
}
