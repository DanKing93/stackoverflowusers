//
//  UsersAPI.swift
//  UserList
//
//  Created by Dan King on 16/03/2020.
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation
import Plexus

enum API {
    
    static var users: Resource<UsersResponse> {
        
        var components = URLComponents()
        
        components.scheme = "http"
        components.host = "api.stackexchange.com"
        components.path = "/2.2/users"
        components.queryItems = [
            URLQueryItem(name: "pagesize", value: "20"),
            URLQueryItem(name: "order", value: "desc"),
            URLQueryItem(name: "sort", value: "reputation"),
            URLQueryItem(name: "site", value: "stackoverflow")
        ]
        
        let request = URLRequest(url: components.url!)
        
        return Resource(request: request) { (data, response) in
            if let error = FetchUsersError(urlResponse: response) { throw error }
            let response = try JSONDecoder().decode(UsersResponse.self, from: data)
            return response
        }
    }
}
