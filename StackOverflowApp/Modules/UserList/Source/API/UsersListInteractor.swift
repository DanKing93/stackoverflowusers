//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation
import Plexus

public protocol UserListInteractor {
    func fetchUsers(_ completion: @escaping (Result<[User], Error>) -> Void)
}

class DefaultUserListInteractor: UserListInteractor {
    
    var urlSession: URLSession
    var task: URLSessionDataTask?
    
    init(urlSession: URLSession = URLSession.sharedSession) {
        self.urlSession = urlSession
    }
    
    func fetchUsers(_ completion: @escaping (Result<[User], Error>) -> Void) {
        
        task?.cancel()
        let resource = Resource.fetchUsers
        task = urlSession.fetch(resource) { result in
            
            DispatchQueue.main.async {
                 completion(result)
            }
        }
    }
}
