//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension API {
    
    struct UsersResponse {
        let users: [API.User]?
    }
}

extension API.UsersResponse: Codable {
    
    enum CodingKeys: String, CodingKey {
        case users = "items"
    }
}
