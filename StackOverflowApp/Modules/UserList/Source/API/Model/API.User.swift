//
//  Copyright © 2020 Dan King. All rights reserved.
//

import Foundation

extension API {
    
    struct User {
        let reputation: Int?
        let profileImageURL: String?
        let displayName: String?
    }
}

extension API.User: Codable {
    
    enum CodingKeys: String, CodingKey {
        case reputation = "reputation"
        case profileImageURL = "profile_image"
        case displayName = "display_name"
    }
}
