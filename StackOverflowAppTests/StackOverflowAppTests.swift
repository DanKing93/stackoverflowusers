//
//  Copyright © 2020 Dan King. All rights reserved.
//

import XCTest

class AppTests: XCTestCase {
    
    func test_somethingToCompile() {
        let string = "String"
        XCTAssertEqual(string, "String")
    }
}
